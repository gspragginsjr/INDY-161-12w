// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import "babel-polyfill";
import Vue from 'vue';
import svg4everybody from 'svg4everybody';
import objectFitImages from 'object-fit-images';
import velocity from 'velocity-animate';
import velocityUI from 'velocity-animate/velocity.ui';
import es6Promise from 'es6-promise';

import GlobalHeader from './GlobalHeader';
import GlobalSubhead from './GlobalSubhead';
import GlobalHero from './GlobalHero';
import GlobalSubhero from './GlobalSubhero';
import GlobalFixedSidebar from './GlobalFixedSidebar';
import GlobalContentWysiwyg from './GlobalContentWysiwyg';
import GlobalBackgroundOne from './GlobalBackgroundOne';
import GlobalBackgroundTwo from './GlobalBackgroundTwo';
import GlobalBackgroundThree from './GlobalBackgroundThree';
import GlobalBackgroundSidebared from './GlobalBackgroundSidebared';
import GlobalPostIndex from './GlobalPostIndex';
import GlobalStory from './GlobalStory';
import GlobalFeaturedContent from './GlobalFeaturedContent';
import GlobalEmblemSpread from './GlobalEmblemSpread';
import GlobalStatSection from './GlobalStatSection';
import GlobalBigBanner from './GlobalBigBanner';
import GlobalBrandBanner from './GlobalBrandBanner';
import GlobalNumberBanner from './GlobalNumberBanner';
import GlobalCarousel from './GlobalCarousel';
import GlobalVideo from './GlobalVideo';
import GlobalVideoExcerpt from './GlobalVideoExcerpt';
import GlobalModal from './GlobalModal';
import GlobalAccordion from './GlobalAccordion';
import GlobalAccordionPanel from './GlobalAccordionPanel';
import GlobalFooter from './GlobalFooter';
import HomeSlider from './HomeSlider';
import HomeSpecialContent from './HomeSpecialContent';
import HomeCta from './HomeCta';
import HomeFooterBanner from './HomeFooterBanner';
import GuideBlock from './GuideBlock';

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {
    GlobalHeader,
    GlobalSubhead,
    GlobalHero,
    GlobalSubhero,
    GlobalFixedSidebar,
    GlobalContentWysiwyg,
    GlobalBackgroundOne,
    GlobalBackgroundTwo,
    GlobalBackgroundThree,
    GlobalBackgroundSidebared,
    GlobalPostIndex,
    GlobalStory,
    GlobalFeaturedContent,
    GlobalEmblemSpread,
    GlobalStatSection,
    GlobalBigBanner,
    GlobalBrandBanner,
    GlobalNumberBanner,
    GlobalCarousel,
    GlobalVideo,
    GlobalVideoExcerpt,
    GlobalModal,
    GlobalAccordion,
    GlobalAccordionPanel,
    GlobalFooter,
    HomeSlider,
    HomeSpecialContent,
    HomeCta,
    HomeFooterBanner,
    GuideBlock
  },
});

svg4everybody();
objectFitImages();
es6Promise.polyfill();

var tag = document.createElement('script');
tag.src = "//www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var looseyScript = document.createElement('script');
looseyScript.appendChild(document.createTextNode('var videoList = []; var readyYoutubeVids = [];'));
firstScriptTag.parentNode.insertBefore(looseyScript, firstScriptTag);

window.onYouTubePlayerAPIReady = function() {

    window.addEventListener("load", function() {

	    for (var video in window.videoList) {
	    	window.readyYoutubeVids.push(new YT.Player(window.videoList[video]));
	    }
	});
}
